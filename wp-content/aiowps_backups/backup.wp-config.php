<?php
/** 
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information by
 * visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress_b');

/** MySQL database username */
define('DB_USER', 'wordpress_d');

/** MySQL database password */
define('DB_PASSWORD', '$LUv05n3Me');

/** MySQL hostname */
define('DB_HOST', 'localhost:3306');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link http://api.wordpress.org/secret-key/1.1/ WordPress.org secret-key service}
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'pMK$ina&75YMkz^H@kNe^VGm7unBTFUyBnJ2YcCIf*3oMnlrHGOMLriMvJ6Addm5');
define('SECURE_AUTH_KEY',  'kmTNJrcO@)trpO&*H3YTe8l*FUBwpvqsrbXp&lLfpjnfZs&NN%u5gZVfWb%YZkA6');
define('LOGGED_IN_KEY',    '5iWhgGUm(SCo%lZ9yYdRVKI!QpLLLcHEhTXu!UjN!ctccH&qU0p1vhzCWrT&Bx21');
define('NONCE_KEY',        'C!j5owuA@P^uL5ncu$$vm05cvMM%8yi1c4wRQnSNW5I5adFMSx&cNRAAREuQTbDc');
define('AUTH_SALT',        'EAAt0b0Uef)Q9klA1L91$qGh)Zc$j6y@Z*jHNGOvxsts8@rr(NKnbFry!n@Os3xa');
define('SECURE_AUTH_SALT', 'EqMqMJ@WvYJZ3epj1Gw!(BF)FOL%RU6Xcr^wQrxjgdJ8zNOdBZd@26XMIN0W^NT6');
define('LOGGED_IN_SALT',   'ZqJ&I&zT^^TWg6)LU$WjwGasZrJ3nCs&yRKIw^pw$DpMVKPnZu&fn4TnUSqWr)4#');
define('NONCE_SALT',       '6G(FN$J&Uf1Wf7mJBw(W5)0oi6&8BHKpM*AZS$V^ZoFbI!$bCyfHaI&bBVMaB&I9');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress.  A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de.mo to wp-content/languages and set WPLANG to 'de' to enable German
 * language support.
 */
define ('WPLANG', 'en_US');

define ('FS_METHOD', 'direct');

define('WP_DEBUG', false);

// Contact 7 - Remove BR tags

define ('WPCF7_AUTOP', false );

define ('WPCF7_LOAD_CSS', false); // Added to disable CSS loading

/* That's all, stop editing! Happy blogging. */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

//--- disable auto upgrade
define( 'AUTOMATIC_UPDATER_DISABLED', true );



?>
