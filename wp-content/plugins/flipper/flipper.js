jQuery(function(){
    jQuery(".card2 a[rel=\'modal:open\']").click(function(event) {
        jQuery(this).modal();
        return false;
    });
    jQuery(".card2").flip({
        axis: "y", // y or x
        reverse: false, // true and false
        trigger: "click", // click or hover
        trigger: "hover", // click or hover
        speed: 500
    });
    var t = (jQuery('#categoryHolder input[type=checkbox]'));
    for (var i = 0; i < t.length; i++) {
        jQuery(jQuery(t[i]).parent()).find("i").hide();
    }
    jQuery('#categoryHolder #catbutton').click(function(){
        var t = (jQuery('#categoryHolder input[type=checkbox]'));

        if ($(this).find('input')[0].checked == true) {
            $(this).find('input')[0].checked = false;
        }
        else {
            $(this).find('input')[0].checked = true;
        }

        var cFalse = [];
        for (var i = 0; i < t.length; i++){
            if (t[i].checked == true) {

                jQuery(jQuery(t[i]).parent()).find("i").hide();
            }
            else {
                cFalse.push(t[i].value);
                jQuery(jQuery(t[i]).parent()).find("i").show();
            }
        }
        var s = jQuery('#card-container article');
        for (var ii = 0; ii < s.length; ii++){
            var bFound = 0;
            jQuery(s[ii]).hide();
            var dataF = jQuery(s[ii]).data('card');
            var dataA = dataF.split(" ");
            for (var oi = 0; oi < dataA.length; oi++){
                for (var op = 0; op < cFalse.length; op++){
                    if (cFalse[op] == dataA[oi])
                        bFound++;
                }
            }
            if (bFound > 0) //== dataA.length)
                jQuery(s[ii]).show();
        }

        t = (jQuery('#categoryHolder input[type=checkbox]'));
        var bCount = 0;
        for (var i = 0; i < t.length; i++){
            if (t[i].checked == false) {
                bCount++;
            }
        }
        if (bCount == 0) {
            s = jQuery('#card-container article');
            for (var ii = 0; ii < s.length; ii++){
                jQuery(s[ii]).show();
            }
        }

        return false;
    });
});