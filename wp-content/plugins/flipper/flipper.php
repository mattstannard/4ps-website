<?php
/**
 * Plugin Name: Flip Cards
 * Plugin URI: http://www.greensquid.co.uk
 * Description: Create Flip Cards
 * Version: 1.0
 * Author: Daniel Newman - GreenSquid Solutions
 * Author URI: http://www.greensquid.co.uk
 * License: A "Slug" license name e.g. GPL2
 */

function FlipCard_register()
{
    $dir = plugins_url() . "/flipper/crown.png";

    $labels = array(
        'name'               => _x( 'Flip Cards', 'Jobs' ),
        'singular_name'      => _x( 'Flip Cards', 'Flip Card' ),
        'add_new'            => _x( 'Add New', 'Flip Card' ),
        'add_new_item'       => __( 'Add New Flip Card' ),
        'edit_item'          => __( 'Edit Flip Card' ),
        'new_item'           => __( 'New Flip Card' ),
        'all_items'          => __( 'All Flip Cards' ),
        'view_item'          => __( 'View Flip Cards' ),
        'search_items'       => __( 'Search Flip Cards' ),
        'not_found'          => __( 'No flip cards found' ),
        'not_found_in_trash' => __( 'No flip cards found in the Trash' ),
        'parent_item_colon'  => '',
        'menu_name'          => 'Flip Cards'
    );

    $args = array(
        'labels'        => $labels,
        'description'   => 'Holds information about the Flip Cards',
        'menu_position' => 5,
        'taxonomies' => array('category', 'post_tag'),
        'supports'      => array( 'title', 'thumbnail', 'author'),
        'menu_icon'   => 'dashicons-controls-repeat',
        'public' => false,  // it's not public, it shouldn't have it's own permalink, and so on
        'publicly_queriable' => true,  // you should be able to query it
        'show_ui' => true,  // you should be able to edit it in wp-admin
        'exclude_from_search' => true,  // you should exclude it from search results
        'show_in_nav_menus' => false,  // you shouldn't be able to add it to menus
        'has_archive' => false,  // it shouldn't have archive page
        'rewrite' => false  // it shouldn't have rewrite rules


    );

    register_post_type( 'flipcard', $args );
}

function FlipCard_admin_init()
{
    // &lt;?php add_meta_box( $id, $title, $callback, $page, $context, $priority ); ?&gt;
    // add_meta_box("Event_Link", "Link to Event", "Event_Link_CB", "4events", "normal", "high");
    add_meta_box("flipcard_front", "Flip Card Front", "flipcard_front_CB", "flipcard", "normal", "high");
    add_meta_box("flipcard_back", "Flip Card Back", "flipcard_back_CB", "flipcard", "normal", "high");
    add_meta_box("flipcard_modal", "Flip Card Modal", "flipcard_modal_CB", "flipcard", "normal", "high");
    register_taxonomy_for_object_type('category', 'page');
}

function flipcard_front_CB()
{
    global $post;
    $custom = get_post_custom($post->id);
    $EventLink = $custom["flipcard_front"][0];

    if ('publish' === get_post_status( get_the_ID() )) {
        $post_shortcode = "[flipcard id='".get_the_ID()."']";
        ?>
        Shortcode: <input type="text" readonly value="<?php echo $post_shortcode; ?>" style="width: 300px;margin-left: 78px;"><br><br>
        <?php
    }

    ?>
    H2 Content: <input type="text" name="flipcard_front" id="flipcard_front" value="<?php echo $EventLink; ?>" style="width: 300px;margin-left: 71px;"><br>
    <strong>H2 Text is White</strong><br>
    <strong>Preferred Featured Image dimensions: 300px width x 200px height</strong>
<?php
}

function flipcard_back_CB()
{
    global $post;
    $custom = get_post_custom($post->id);

    $EventLink = $custom["flipcard_back1"][0];
    ?>
    <?php
    wp_editor( $EventLink, 'flipcard_back1', array("media_buttons" => false) );
    ?>
    <div style="float:right;" ><span id="chars">100</span> characters remaining</div><br>
    <script>
        var maxLength = 100;
        jQuery(function() {
            jQuery('textarea').keyup(function() {
                var length = jQuery(this).val().length;
                var length = maxLength-length;
                jQuery('#chars').text(length);
            });
        });
    </script>
<?php
}

function flipcard_modal_CB()
{
    global $post;
    $custom = get_post_custom($post->id);

    $EventLink = $custom["flipcard_modal1"][0];
    ?>
    <?php
    wp_editor( $EventLink, 'flipcard_modal1', array("media_buttons" => true) );
    ?>
    <div style="float:right;" ><span id="chars">100</span> characters remaining</div><br>
<?php
}

function flipcard_creation( $atts ) {
    shortcode_atts( array(
        'id' => NULL,
        'title' => 'testing',
        'category' => 'testing',
        'tag' => 'testing',
    ), $atts );
    $postID = (int) $atts['id'];
    //$postID = 19138;
    $moo = "";
    $custom_fields = get_post_custom($postID);
    $featuredImage = wp_get_attachment_image_src($custom_fields['_thumbnail_id'][0], 'featured-home-thumb');
	$featuredImage = $featuredImage[0];
    $alt_text = get_post_meta($custom_fields['_thumbnail_id'][0] , '_wp_attachment_image_alt', true);

    $title = $custom_fields['flipcard_front'][0];
    $backtext = $custom_fields['flipcard_back1'][0];

    $modalText = $custom_fields['flipcard_modal1'][0];
    $rest = "";
    if (strpos($backtext,'[flipcard_modal') !== false) {
        $pos1 = strpos($backtext,'[flipcard_modal');
        $pos2 = strpos($backtext,']', $pos1);

        //[flipcard_modal link='something' alt='somethingelse']
        $rest = substr($backtext, $pos1, $pos2+1);
        $linkText = "default";
        $altText = "default";

        if (strpos($rest,'link=') !== false) {
            $pos1a = strpos($rest,"link='")+6;
            $pos2a = strpos($rest,"'", $pos1a);
            $linkText = substr($rest, $pos1a, $pos2a-$pos1a);
        }
        if (strpos($rest,'alt=') !== false) {
            $pos1b = strpos($rest,"alt='")+5;
            $pos2b = strpos($rest,"'", $pos1b);
            $altText = substr($rest, $pos1b, $pos2b-$pos1b);
        }

        $modalLink = '<a  href="#modal'.$postID.'" alt="'.$altText.'">'.$linkText.'</a>';
        $backtext = str_replace($rest, $modalLink, $backtext);
    }

    $flipcard = '<article class="card2">
        <header class="front">
            <img alt="'.$alt_text.'" src="'.$featuredImage.'" />
            <h2>'.$title.'</h2>
        </header>
        <footer class="back">
            <p>'.$backtext.'</p>
        </footer>
    </article>';

    $modal = '<div class="remodal" data-remodal-id="modal'.$postID.'" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
    <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
    <div>'.$modalText.'</div></div>';

    //$modal = '<div id="ex'.$postID.'" class="modal" style="display:none;  width: 590px;">'.$modalText.'<a href="#close-modal" rel="modal:close" class="close-modal ">Close</a></div>';
    return $moo.''.$flipcard . ' ' .$modal;
}

// add more buttons to the html editor
function appthemes_add_quicktags() {
    if (get_post_type() == "flipcard") {
        if (wp_script_is('quicktags')){
            ?>
            <script type="text/javascript">
                QTags.addButton( 'eg_hr2', 'Insert Flip Card Modal', "[flipcard_modal link='' alt='']", '', 'h2', 'Flip Card Modal', 202 );
            </script>
        <?php
        }
    }
    else {
        if (wp_script_is('quicktags')){
            ?>
            <script type="text/javascript">
                QTags.addButton( 'eg_hr1', 'FlipCard', "[flipcard id='']", '', 'h1', 'Flip Card', 201 );
            </script>
        <?php
        }
    }

}

function flipcard_admin_save()
{
    global $post;
    update_post_meta($post->ID, "flipcard_front", $_POST["flipcard_front"]);
    update_post_meta($post->ID, "flipcard_back1", $_POST["flipcard_back1"]);
    update_post_meta($post->ID, "flipcard_modal1", $_POST["flipcard_modal1"]);
}

// ADD NEW COLUMN
function flipper_columns_head($defaults) {
    $defaults['shortcode'] = 'Shortcode';
    return $defaults;
}

// SHOW THE FEATURED IMAGE
function flipper_columns_content($column_name, $post_ID) {
    if ($column_name == 'shortcode') {
        $post_shortcode = "[flipcard id='".$post_ID."']";
        echo $post_shortcode;
    }
}

function flipcards ($categoriesFromPage) {
    $args = array(

        'orderby' => 'post_date',
        'order' => 'DESC',
        'post_type' => 'flipcard',
        'post_status' => 'publish',
        'posts_per_page' => -1
    );

    $tagS1 = array();
    $newCat = array();


    $wp_query = new WP_Query( $args );
    if ( $wp_query->have_posts() ) :
        $outputstart = '<section class="section section--gutters section--alternative" id="card-container"><div class="grid__primary-container">';
        $output = "";
        while ( $wp_query->have_posts() ) : $wp_query->the_post();
           // $output .= get_the_title();
            $postID = get_the_ID();
            //var_dump($postID);
            $custom_fields = get_post_custom($postID);
            //var_dump($custom_fields);
            $featuredImage = wp_get_attachment_image_src($custom_fields['_thumbnail_id'][0], 'featured-home-thumb');
			$featuredImage = $featuredImage[0];
            $alt_text = get_post_meta($custom_fields['_thumbnail_id'][0] , '_wp_attachment_image_alt', true);

            $title = $custom_fields['flipcard_front'][0];
            $backtext = $custom_fields['flipcard_back1'][0];

            $modalText = $custom_fields['flipcard_modal1'][0];
            $rest = "";
            if (strpos($backtext,'[flipcard_modal') !== false) {
                $pos1 = strpos($backtext,'[flipcard_modal');
                $pos2 = strpos($backtext,']', $pos1);

                //[flipcard_modal link='something' alt='somethingelse']
                $rest = substr($backtext, $pos1, $pos2+1);
                $linkText = "default";
                $altText = "default";

                if (strpos($rest,'link=') !== false) {
                    $pos1a = strpos($rest,"link='")+6;
                    $pos2a = strpos($rest,"'", $pos1a);
                    $linkText = substr($rest, $pos1a, $pos2a-$pos1a);
                }
                if (strpos($rest,'alt=') !== false) {
                    $pos1b = strpos($rest,"alt='")+5;
                    $pos2b = strpos($rest,"'", $pos1b);
                    $altText = substr($rest, $pos1b, $pos2b-$pos1b);
                }

                $modalLink = '<a  href="#modal'.$postID.'" alt="'.$altText.'">'.$linkText.'</a>';
                $backtext = str_replace($rest, $modalLink, $backtext);
            }

            $categories = get_the_terms($postID, "category");
            $tags = get_the_terms($postID, "post_tag");
            $catText = "";
            $tagClass = "";
            $tagName = "";
            if (is_array($categories) || is_object($categories)) {
                foreach ($categories as $cat1) {
                    foreach ($categoriesFromPage as $cat2) {

                        if ($cat1->slug == $cat2->slug) {
                            if (!empty($tags)) {
                                foreach ($tags as $cat) {
                                    $ar = array("slug" => $cat->slug, "name" => $cat->name);
                                    array_push($tagS1, $ar); //$cat->slug);
                                    $catText .= $cat->slug . " ";
                                   // $tagS1 = array_unique($tagS1);
                                    $tagS1 = super_unique($tagS1,'slug');
                                }
                                $catText = trim($catText);

                                $flipcard = '<article class="card2" data-card="' . $catText . '">
                                                <!-- updated to add darkness to images -->
						<header class="front" style="background-image: linear-gradient( to bottom, rgba(0, 0, 0, 0.1) 0%, rgba(0, 0, 0, 0.4) 100% ), url('.$featuredImage.');">
                                                    <!-- <img alt="' . $alt_text . '" src="' . $featuredImage . '" /> -->
                                                    <h2>' . $title . '</h2>
                                                </header>
                                                <footer class="back">
                                                    <p>' . $backtext . '</p>
                                                </footer>
                                            </article>';

                                $modal = '<div class="remodal" data-remodal-id="modal' . $postID . '" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
                                            <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
                                            <div>' . $modalText . '</div></div>';

                                $output .= $flipcard . ' ' . $modal;
                            }
                        }
                    }
                }
            }

            foreach ($tagS1 as $cat3) {
                $tagClass .= "<div id='catbutton'><label><input id='catButtons' type='checkbox' checked value='".$cat3['slug']."' /><i class='fa fa-check'></i> ".$cat3['name']."</label></div>";
            }
        endwhile;






//        foreach ($newCat as $cat) {
//            $catClass .= "<div id='catbutton'><label><input id='catButtons' type='checkbox' checked value='".$cat."' /><i class='fa fa-check'></i> ".$cat."</label></div>";
//           // $catClass2 .= "<option>".$cat."</option> ";
//        }
        //$catClass2 = "<select>".$catClass2."</select>";

        echo $outputstart."<div id='categoryHolder' style='  margin-left: auto;  margin-right: auto;  display: table;'>".$tagClass."</div><br><div class='cardHolder'>".$output. '</div></div></section>';
        wp_reset_query();

    endif;

    echo "<div style='clear:both;'></div>";
}

add_action('init', 'FlipCard_register');
add_action("admin_init", "FlipCard_admin_init");
add_action('save_post', 'flipcard_admin_save');
add_action( 'admin_print_footer_scripts', 'appthemes_add_quicktags' );
add_shortcode( 'flipcard', 'flipcard_creation' );

add_filter('manage_flipcard_posts_columns', 'flipper_columns_head');
add_action('manage_flipcard_posts_custom_column', 'flipper_columns_content', 0, 2);

function super_unique($array,$key)
{
    $temp_array = array();
    foreach ($array as &$v) {
        if (!isset($temp_array[$v[$key]]))
            $temp_array[$v[$key]] =& $v;
    }
    $array = array_values($temp_array);
    return $array;
}


?>