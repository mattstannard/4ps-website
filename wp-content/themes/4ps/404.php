<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage 4Ps
 * @since 4Ps 1.0
 */

get_header(); ?>

	<header class="hero" role="banner">
		<div class="grid__primary-container">
			<div class="grid__full">
				<div class="flex__hero">

                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="4Ps Marketing - Home">
                        <img class="hero__logo" src="<?php echo get_template_directory_uri(); ?>/img/4ps-logo.svg" alt="<?php bloginfo( 'name' ); ?> - Logo">
                    </a>
					<h1 class="hero__strapline"><?php _e( '404 Not Found', 'twentyfourteen' ); ?></h1>
					<p class="hero__proposition"><?php _e( 'Unfortunately this content could not be found, please use the main navigation or try a search below.', 'twentyfourteen' ); ?></p>

					<?php get_search_form(); ?>

				</div>
			</div>
		</div>
	</header>

<?php
get_footer();
