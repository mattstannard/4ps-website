<?php
/**
 * 4Ps functions and definitions
 *
 */

if ( ! function_exists( 'fourps_setup' ) ) :
/**
 * 4Ps setup
 */
	function fourps_setup() {

		// Add RSS feed links to <head> for posts and comments.
		add_theme_support( 'automatic-feed-links' );

		// Enable support for Post Thumbnails
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 1680, 1050, true );
		add_image_size( 'client-horizontal-list-list', 200, 100 );

		// add_image_size( 'icon-x-large-2x', 600, 600 );
		// add_image_size( 'icon-large-2x', 500, 500 );
		//
		// add_image_size( 'icon-x-large', 300, 300 );
		// add_image_size( 'icon-large', 250, 250 );

		// Register the menus
		register_nav_menus( array(
			'primary'   => 'Top primary menu',
			'footer'    => 'Footer menu',
		) );

		add_editor_style( array( 'css/editor-style.css' ) );

		add_theme_support( 'html5', array(
			'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
		) );

	}
endif;
add_action( 'after_setup_theme', 'fourps_setup' );


/**
* Remove p tag wrap on term description
*/


remove_filter('term_description','wpautop');

add_filter( 'jpeg_quality', function($arg){return 100;});
add_filter( 'wp_editor_set_quality', function($arg){return 100;} );

/**
* Allow SVG uploads
*/

function cc_mime_types( $mimes ){
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );

/**
* Create custom post types
*/

add_action( 'init', 'create_post_type' );
function create_post_type() {
	register_post_type( '4ps__client',
		array(
			'labels' => array(
				'name' => __( 'Clients' ),
				'singular_name' => __( 'Client' )
			),
			'public' => true,
			'has_archive' => false,
			'exclude_from_search' => true,
			'rewrite' => array(
				'slug' => __( 'clients' )
			)
		)
	);
}

/**
* Register service taxonomy
*/

add_action( 'init', 'fourps_service_tax' );
function fourps_service_tax() {
	register_taxonomy(
		'services',
		'service',
		array(
			'label' => __( 'Sevice' ),
			'rewrite' => array( 'slug' => 'Service' ),
			'hierarchical' => true,
		)
	);

	if ( !term_exists( 'Paid Search', 'services' ) ) :
		wp_insert_term(
			'Paid Search',
			'services',
			array(
				'description'=> 'Paid search service',
				'slug' => 'paid-search'
			)
		);
	endif;

	if ( !term_exists( 'SEO Services', 'services' ) ) :
		wp_insert_term(
			'SEO Services',
			'services',
			array(
				'description'=> 'SEO services',
				'slug' => 'seo-services'
			)
		);
	endif;
}



/**
* Register sector taxonomy
*/

add_action( 'init', 'fourps_sector_tax' );
function fourps_sector_tax() {
	register_taxonomy(
		'sectors',
		'sector',
		array(
			'label' => __( 'Sectors' ),
			'rewrite' => array( 'slug' => 'Sector' ),
			'hierarchical' => true,
		)
	);

	if ( !term_exists( 'B2B', 'sectors' ) ) :
		wp_insert_term(
			'B2B',
			'sectors',
			array(
				'description'=> 'B2B',
				'slug' => 'b2b'
			)
		);
	endif;

	if ( !term_exists( 'B2C', 'sectors' ) ) :
		wp_insert_term(
			'B2C',
			'sectors',
			array(
				'description'=> 'B2C',
				'slug' => 'b2c'
			)
		);
	endif;

	if ( !term_exists( 'Third Sector', 'sectors' ) ) :
		wp_insert_term(
			'Third Sector',
			'sectors',
			array(
				'description'=> 'Third Sector',
				'slug' => 'third-sector'
			)
		);
	endif;

	if ( !term_exists( 'Professional Services', 'sectors' ) ) :
		wp_insert_term(
			'Professional Services',
			'sectors',
			array(
				'description'=> 'Professional Services',
				'slug' => 'professional-services'
			)
		);
	endif;

	if ( !term_exists( 'Business Services', 'sectors' ) ) :
		wp_insert_term(
			'Business Services',
			'sectors',
			array(
				'description'=> 'Business Services',
				'slug' => 'business-services'
			)
		);
	endif;

	if ( !term_exists( 'Education', 'sectors' ) ) :
		wp_insert_term(
			'Education',
			'sectors',
			array(
				'description'=> 'Education',
				'slug' => 'education'
			)
		);
	endif;

	if ( !term_exists( 'Publishing', 'sectors' ) ) :
		wp_insert_term(
			'Publishing',
			'sectors',
			array(
				'description'=> 'Publishing',
				'slug' => 'publishing'
			)
		);
	endif;

	if ( !term_exists( 'Food and drink', 'sectors' ) ) :
		wp_insert_term(
			'Food and drink',
			'sectors',
			array(
				'description'=> 'Food and drink',
				'slug' => 'food-drink'
			)
		);
	endif;

	if ( !term_exists( 'Fashion', 'sectors' ) ) :
		wp_insert_term(
			'Fashion',
			'sectors',
			array(
				'description'=> 'Fashion',
				'slug' => 'fashion'
			)
		);
	endif;

	if ( !term_exists( 'Beauty', 'sectors' ) ) :
		wp_insert_term(
			'Beauty',
			'sectors',
			array(
				'description'=> 'Beauty',
				'slug' => 'beauty'
			)
		);
	endif;

	if ( !term_exists( 'Insurance', 'sectors' ) ) :
		wp_insert_term(
			'Insurance',
			'sectors',
			array(
				'description'=> 'Insurance',
				'slug' => 'insurance'
			)
		);
	endif;

	if ( !term_exists( 'Travel', 'sectors' ) ) :
		wp_insert_term(
			'Travel',
			'sectors',
			array(
				'description'=> 'Travel',
				'slug' => 'travel'
			)
		);
	endif;
}

/**
 * Register widget areas
 */

add_action( 'widgets_init', 'fourps_widgets_init' );
function fourps_widgets_init() {
	register_sidebar( array(
		'name' => 'Blog widget area',
		'id' => 'blog-main',
		'before_widget' => '<div>',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="rounded">',
		'after_title' => '</h2>',
	) );
}


add_filter( 'request', 'my_request_filter' );
function my_request_filter( $query_vars ) {
    if( isset( $_GET['s'] ) && empty( $_GET['s'] ) ) {
        $query_vars['s'] = " ";
    }
    return $query_vars;
}


function the_breadcrumbs() {
	global $post;
	global $query_string;

	// If page has query string - break it up into array

	if ( $query_string ) :

		$queryArgs = explode("&", $query_string);
		$searchQuery = array();

		foreach( $queryArgs as $key => $string ) :
			$querySplit = explode("=", $string);
			$searchQuery[ $querySplit[0] ] = urldecode( $querySplit[1] );
		endforeach;

	endif;


	$output = '<ul class="breadcrumbs" itemprop="breadcrumb">';

		if ( is_page() ) :

			$output .= '<li class="breadcrumbs__item"><a class="breadcrumbs__link" href="' . get_option( 'home' ) . '">Home</a> <span class="fa fa-chevron-right pull-right pull-left"></span></li>';

			if ( $post->post_parent ) :
				$ancestors = get_post_ancestors( get_the_ID() );

				foreach ( $ancestors as $ancestorID ) :
					$output .= '<li class="breadcrumbs__item"><a class="breadcrumbs__link" href="' . get_permalink( $ancestorID ) . '" title="' . get_the_title( $ancestorID ) . '">' . get_the_title( $ancestorID ) . '</a> <span class="fa fa-chevron-right pull-right pull-left"></span></li>';
				endforeach;

			endif;

			$output .= '<li class="breadcrumbs__item">' . get_the_title() . '</li>';

		else :

			$categories = get_the_category();

			$output .= '<li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/blog" title="Our Blog">Our Blog</a> <span class="fa fa-chevron-right pull-right pull-left"></span></li>';

			if ( is_category() ) :

				$output .= '<li class="breadcrumbs__item">' . single_cat_title('',false) . '</li>';

			elseif ( is_single() ) :
					$output .= '<li class="breadcrumbs__item"><a class="breadcrumbs__link"  href="' . get_category_link( $categories[0]->term_id ) .'" title="' . $categories[0]->name . '">' . $categories[0]->name . '</a>  <span class="fa fa-chevron-right pull-right pull-left"></span></li>';
					$output .= '<li class="breadcrumbs__item">Blog</li>';

			elseif ( is_search() ) :

				if ( trim( $searchQuery['s'] ) ) :
					$output .= '<li class="breadcrumbs__item">Search: ' . trim( $searchQuery['s'] ) . '</li>';
				else :
					$output .= '<li class="breadcrumbs__item">Search</li>';
				endif;
			endif;

		endif;

	$output .= '</ul>';

	echo $output;
}




function the_clients( $currentPageId, $type, $hasWrapper = true ) {
	global $post;

	$args = array(
		'post_type'     => '4ps__client',
		'orderby'       => 'menu_order',
		'order'         => 'DESC'
	);

//	$args = array(
//		'post_type'     => '4ps__client',
//		'orderby'       => 'menu_order',
//		'order'         => 'ASC'
//	);

	$query = new WP_Query( $args );

	if ( $query->have_posts() ) :

		$leadWrapper = '';
		$endWrapper = '';
		$output = '';
		$i = 0;

		while ( $query->have_posts() ) : $query->the_post();

			if ( $currentPageId ) :

				switch ( $type ) {

					case 'light' :
						$image = get_field( '4ps_client_logo_dark' );
					break;

					case 'dark' :
						$image = get_field( '4ps_client_logo_light' );
					break;

					default :
						$image = get_field( '4ps_client_logo_dark' );
					break;
				}

				$showOnPageIds = get_field( '4ps_client_show_in' );
				
				if ( $image && $showOnPageIds ) :

					$link = get_field( '4ps_client_case_study' );
					//$output .= '<div style="display:none">' . var_dump($currentPageId) . ' : ' . var_dump($showOnPageIds) . '</div>';
					if ( in_array($currentPageId, $showOnPageIds ) ) :
						$output .= '<div class="flex__client-item">';

						if ( $link ) :
							$output .= '<a class="client-list__client" href="' . $link . '" title="'.get_the_title().'">';
						endif;

						$output .= '<img class="client-list__logo" src="' . $image['sizes']['client-horizontal-list-list'] . '" alt="' . get_the_title() . '">';

						if ( $link ) :
							$output .= '</a>';
						endif;

						$output .= '</div>';

						$i++;

					endif;
				endif;
			endif;
		endwhile;

		wp_reset_postdata();

		if ( $i > 0 ) :

			if ( $hasWrapper ) :
				$leadWrapper .= '<div class="client-list ';
				if ( $type == 'dark' ) :
					$leadWrapper .= 'client-list--dark';
				else :
					$leadWrapper .= 'client-list--light';
				endif;
				$leadWrapper .= '">';
				$leadWrapper .=	'<div class="grid__primary-container">';
			endif;

			$leadWrapper  .=  '<div class="flex__client-list">';
			$endWrapper   .= '</div>';

			if ( $hasWrapper ) :
				$endWrapper  .= '</div>';
				$endWrapper .= '</div>';
			endif;

			$output = $leadWrapper.$output.$endWrapper;

		endif;

	endif;

	echo $output;
}





function the_cards( $filters = false, $showHeader = true, $pagination = true ) {

	// Get the globals

	global $wp_query;
	global $post;
	global $paged;
	global $query_string;

	// Pagination black magic

	if ( get_query_var('paged') ) :
		$paged = get_query_var('paged');
	elseif ( get_query_var('page') ) :
		$paged = get_query_var('page');
	else :
		$paged = 1;
	endif;

	// If page has query string - break it up into array

	if ( $query_string ) :

		$queryArgs = explode("&", $query_string);
		$searchQuery = array();

		foreach( $queryArgs as $key => $string ) :
			$querySplit = explode("=", $string);
			$searchQuery[ $querySplit[0] ] = urldecode( $querySplit[1] );
		endforeach;

	endif;

	// Check if list is to display featured cards only

	$featured = get_field( '4ps_show_featured_cards_only' );

	/**
	 * Let's create the paramaters for getting the cards
	 */

	// Some default attributes

	$args = array(

		'orderby' => 'post_date',
		'order' => 'DESC',
		'post_type' => 'post',
		'post_status' => 'publish',
		'suppress_filters' => true,
		'posts_per_page' => 9
		//'post__not_in' => array($post->ID)
	);

	// Show only featured cards ?

	if ( $featured ) :
		$featuredArray = array( 'meta_key' => '4ps_card_featured', 'meta_value' => true );
		$args = array_merge( $args, $featuredArray );
	endif;

	// Filter by category if available

	if ( $filters ) :

		foreach( $filters as $filter ) :

			$string = '';

			$i = 1;

			foreach ( $filter[1] as $value ) :

				$string .= $value;

				if ( $i < count( $filter[1] ) ) :
					$string .= ',';
				endif;

				$i++;

			endforeach;


			switch ( $filter[0] ) :

				case 'category' :

					$args['category_name'] = $string;

				break;

				case 'tag' :

					$args['tag'] = $string;

				break;

			endswitch;

		endforeach;

	endif;


	// Make sure pagination works

	if ( $paged ) :
		$args['paged'] = $paged;
	endif;

	// If keyword is search based.. merge into args

	if ( $searchQuery['s'] ) :
		$args = array_merge( $args, $searchQuery );
	endif;

	// Finally run the query!

	$wp_query = new WP_Query( $args );

	/**
     * Start to render
	 */

	$category = get_category_by_slug( $categories );

	if ( $category ) :
		$heading = $category->name;
		$description = $category->description;
	else :
		$heading = 'OUR BLOG';
		$description = 'Keep up-to-date with 4Ps latest news, events and social activities.';
	endif;

	if ( $wp_query->have_posts() ) :

		$output .= '<section class="section section--gutters section--alternative" id="card-container">';

		if ( $showHeader ) :

			$output .= '<div class="grid__primary-container">';
			$output .= '<div class="grid__full">';
			$output .= '<h2 class="section__heading">' . $heading . '</h2>';
			$output .= '<p class="section__intro">' . $description . '</p>';
			$output .= '</div>';
			$output .= '</div>';

		endif;

		$output .= '<div class="grid__primary-container">';

		while ( $wp_query->have_posts() ) : $wp_query->the_post();

			$output .= '<div class="grid__card">';
				$output .= '<a class="card card--link" href="' . get_permalink( get_the_ID() ) . '" title="' . get_the_title() . '">';
					$categories = get_the_category();
                    $iconImage = "/4ps-news.png";
                    if ( $categories ) {
                        if ($categories[0]->name == "4Ps News") {
                            $iconImage = "/4ps-news.png";
                        }
                        else if ($categories[0]->name == "Search Industry News"){
                            $iconImage = "/industry-sector.png";
                        }
                        else if ($categories[0]->name == "Market Sectors"){
                            $iconImage = "/search-news.png";
                        }
                        else if ($categories[0]->name == "4Ps Labs"){
                            $iconImage = "/4ps-labs.png";
                        }
                        else if ($categories[0]->name == "Knowledge"){
                            $iconImage = "/search-knowledge.png";
                        }
                        else if ($categories[0]->name == "Case Studies"){
                            $iconImage = "/case-studies.png";
                        }
                    }
                    $output .= '<p style="  clear: both;" class="card__heading">';
                    if ( get_field( '4ps_card_custom_title' ) ) :
                        $output .= get_field( '4ps_card_custom_title' );
                    else :
                        $output .= get_the_title();
                    endif;
                    $output .= '</p>';
					$output .= '<img class="card__icon" src="'.$iconImage.'">';
//					$output .= '<p class="card__category">';
//						if ( $categories ) :
//							$output .= $categories[0]->name;
//						else :
//							$output .= 'Page';
//						endif;
//					$output .= '</p>';

                    $postID = get_the_ID();
                    $custom_fields = get_post_custom($postID);
                    $featuredImage = wp_get_attachment_image_src($custom_fields['_thumbnail_id'][0], 'featured-home-thumb');
					$featuredImage = $featuredImage[0];

                    if ($featuredImage != "") {
                        $alt_text = get_post_meta($custom_fields['_thumbnail_id'][0] , '_wp_attachment_image_alt', true);
                        $output .= '<img class="featured__image" alt="'.$alt_text.'" src="'.$featuredImage.'" />';
                    }

					$output .= '<p class="card__excerpt">';
						if ( get_field( '4ps_card_custom_summary', get_the_ID() ) ) :
							$output .= get_field( '4ps_card_custom_summary' );
						else :
							$output .= get_field( '4ps_header_introduction' );
						endif;
					$output .= '</p>';
				$output .= '</a>';
			$output .= '</div>';

		endwhile;

		$output .= '</div>';
		$output .= '</section>';

		// Only show pagination if featured cards is not true and there are 9 cards or more

		if ( $pagination ) :
			if ( !$featured && ( $wp_query->post_count >= 9 ) ) :

			$output .= '<nav id="blog-home-pagination" class="page-navigation section--gutters">';
			$output .= '	<div class="grid__primary-container">';
			$output .= '		<div class="grid__previous-link">';
			$output .= '			<div class="page-navigation__item page-navigation__item--long page-navigation__previous">';
			$output .= 				get_previous_posts_link( '<span class="fa fa-chevron-left"></span> View Previous' );
			$output .= '			</div>';
			$output .= '			<div class="page-navigation__item page-navigation__item--short page-navigation__previous">';
			$output .= 				get_previous_posts_link( '<span class="fa fa-chevron-left"></span> Previous' );
			$output .= '			</div>';
			$output .= '		</div>';
			$output .= '		<div class="grid__next-link">';
			$output .= '			<div class="page-navigation__item page-navigation__item--long page-navigation__next">';
			$output .= 				get_next_posts_link( 'View Next <span class="fa fa-chevron-right"></span>' );
			$output .= '			</div>';
			$output .= '			<div class="page-navigation__item page-navigation__item--short page-navigation__next">';
			$output .= 				get_next_posts_link( 'Next <span class="fa fa-chevron-right"></span>' );
			$output .= '			</div>';
			$output .= '		</div>';
			$output .= '	</div>';
			$output .= '</nav>';

			endif;
		endif;

		echo $output;
		wp_reset_query();

	endif;
}
