<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage 4Ps
 * @since 4Ps 1.0
 */

get_header(); ?>

<!-- Header block -->
<header class="hero hero--alternative" role="banner">
	<div class="grid__primary-container">
		<div class="grid__full">
			<div class="flex__hero">

				<!-- Hero text block -->
				<h1 class="hero__strapline"><?php the_title(); ?></h1>
				<p class="hero__proposition"><?php (get_field('4ps_header_introduction') ? the_field('4ps_header_introduction') : ''); ?></p>
				<!-- /Hero text block -->

				<!-- Get in touch block -->
				<a href="#comment-form" class="hero__button hero__button--light" title="Get in touch with 4Ps"><?php echo (get_field('4ps_header_button') ? get_field('4ps_header_button') : 'Get in touch'); ?></a>
				<!-- /Get in touch block -->

				<a href="#main-content" class="hero__arrow" title="Go to main content"><img src="<?php echo get_template_directory_uri(); ?>/img/arrow-down.png" alt=""></a>

			</div>
		</div>
	</div>
</header>
<!-- /Hero block -->


<?php if ( have_posts() ) :	?>

<section class="section section--gutters section--alternative">
	<div class="grid__primary-container">
		<div class="grid__full">
			<h2 class="section__heading">Our Blog</h2>
			<p class="section__intro">Keep up-to-date with 4Ps latest news, studies, R&D, events and social activities.</p>
		</div>
	</div>
	<div class="grid__primary-container">
	<?php
		// Start the Loop.
		while ( have_posts() ) : the_post();
			get_template_part( '4ps', 'module-cards' );
		endwhile;
	?>
	</div>
</section>
<?php else: ?>


	NO RESULTS


<?php endif; ?>


<?php
get_footer();
