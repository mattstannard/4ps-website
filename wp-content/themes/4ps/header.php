<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage 4Ps
 * @since 4Ps 1.0
 */
?><!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?><?php bloginfo('name'); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/vendors/font-awesome/font-awesome.css">
    <link rel="stylesheet" href="<?php echo plugins_url(); ?>/flipper/remodal.css">
    <link rel="stylesheet" href="<?php echo plugins_url(); ?>/flipper/remodal-default-theme.css">
    <link rel="stylesheet" href="<?php echo plugins_url(); ?>/flipper/flipper.css" />
<link rel="shortcut icon" href="/favicon.ico">
<?php wp_head(); ?>
<script type="text/javascript"> var _ss = _ss || []; _ss.push(['_setDomain', 'https://koi-1MYZXA6.sharpspring.com/net']); _ss.push(['_setAccount', 'KOI-23MQF2Q']); _ss.push(['_trackPageView']); (function() { var ss = document.createElement('script'); ss.type = 'text/javascript'; ss.async = true; ss.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'koi-1MYZXA6.sharpspring.com/client/ss.js?ver=1.1.1'; var scr = document.getElementsByTagName('script')[0]; scr.parentNode.insertBefore(ss, scr); })(); </script>


</head>

<body <?php body_class(); ?> itemscope itemtype="http://schema.org/<?php echo (isset($schemaItemType) ? $schemaItemType : 'WebPage'); ?>">
	<?php
		$postType = get_post_type();
		$categories = get_the_category();

		if ( $categories ) :
			$primaryType = $categories[0]->name;
		elseif ( $postType ) :
			$primaryType = $postType;
		endif;
	?>

	<?php
		if ( get_field( '4ps_service_tax' ) ) :
			$serviceTax = get_term_by( 'id', get_field( '4ps_service_tax' ), 'services');
		else:
			$serviceTax = null;
		endif;

		if ( get_field( '4ps_sector_tax' ) ) :
			$sectorTax = get_term_by( 'id', get_field( '4ps_sector_tax' ), 'sectors');
		else:
			$sectorTax = null;
		endif;
	?>

	<!-- W3 Data Layer -->
	<script type="text/javascript">
		dataLayer = [];
		digitalData = {
			event:[],
			pageInstanceID: "<?php echo $post->post_name; ?>",
			page: {
				pageID: "<?php echo $post->ID; ?>",
				destinationURL: document.location,
				issueDate: "<?php echo $post->post_date; ?>",
				author: "<?php echo get_the_author_meta( 'display_name', $post->post_author ); ?>",
				attributes: {
					sector: "<?php echo $sectorTax->name; ?>",
					service: "<?php echo $serviceTax->name; ?>"
				},
				<?php if ( $primaryType ) : ?>
					category:{
						primaryCategory: "<?php echo $primaryType; ?>"
					}
				<?php endif; ?>
			}
		}
		dataLayer.push({'digitalData':digitalData});
	</script>
	<!-- /W3 Data Layer -->

	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P7X44X"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-P7X44X');</script>
	<!-- /Google Tag Manager -->
	<?php

	$slug = get_page_template_slug( get_the_ID() );
	$noHero = false;

	if ( $slug == 'page-templates/blog.php' ) :
		$noHero = true;
	endif;
	?>
	<div id="page">
		<div id="skybar" class="skybar <?php echo ( !$noHero && ( wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) ) && is_page() ) ) ? 'skybar--alternative' : ''; ?>">
			<a class="skybar__link" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="4Ps Marketing - Home">
				<img class="skybar__logo" src="<?php echo get_template_directory_uri(); ?>/img/4ps-mini-logo.svg" alt="<?php bloginfo( 'name' ); ?> - Logo">
			</a>
			<div class="skybar__nav-toggle">
				<span class="fa fa-bars fa-2x" id="primary-navigation-open"></span>
			</div>
			<nav id="primary-navigation" class="primary-navigation" role="navigation">
				<button class="primary-navigation__close" id="primary-navigation-close"></button>
				<div class="grid__navigation-wrapper">
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
				</div>
			</nav>
		</div>
