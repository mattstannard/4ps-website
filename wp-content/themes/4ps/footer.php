<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage 4Ps
 * @since 4Ps 1.0
 */
?>
		<div id="comment-form" class="section section--gutters section--comment-form">
			<div class="grid__primary-container">
				<p class="comment__heading">Get in touch</p>
				<?php echo do_shortcode('[contact-form-7 id="16756" title="Online Contact Form"]'); ?>
			</div>
		</div>
		<footer class="footer section section--footer">
			<div class="grid__primary-container">
				<div class="grid__footer-navigation">
					<nav class="footer-navigation">
						<?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_class' => 'nav-menu' ) ); ?>
					</nav>
				</div>
				<div class="grid__footer-contact-info">
					<div class="footer__contact-block">
						<p class="footer__heading">Think we can help? <span class="footer__heading--second-line">Get in touch</span></p>
						<ul class="footer__contact">
							<li class="footer__contact-item"><a class="footer__contact-link" href="tel:+442076075650">+44 (0)207 607 5650</a></li>
							<li class="footer__contact-item"><a class="footer__contact-link footer__contact-link--email" href="mailto:hello@4psmarketing.com">hello@4psmarketing.com</a></li>
						</ul>
						<div class="footer__social-media">
							<a rel="me" class="footer__social-link footer__social-link--facebook" href="//www.facebook.com/4psmarketing" title="4Ps on Facebook" target="blank"><span class="fa fa-facebook-square fa-2x"></span></a>
							<a rel="me" class="footer__social-link footer__social-link--twitter" href="//twitter.com/4PsMarketing" title="4Ps on Twitter" target="blank"><span class="fa fa-twitter-square fa-2x"></span></a>
							<a rel="me" class="footer__social-link footer__social-link--linkedin" href="//www.linkedin.com/company/4ps-marketing_2" title="4Ps on LinkedIn" target="blank"><span class="fa fa-linkedin-square fa-2x"></span></a>
							<a rel="me" class="footer__social-link footer__social-link--googleplus" href="//plus.google.com/+4psmarketingagency/posts" title="4Ps on Google+" target="blank"><span class="fa fa-google-plus-square fa-2x"></span></a>
							<a rel="me" class="footer__social-link footer__social-link--youtube" href="//www.youtube.com/user/4psmarketing" title="4Ps on Youtube" target="blank"><span class="fa fa-youtube-square fa-2x"></span></a>
						</div>
					</div>
				</div>
			</div>
			<div class="awardfooter">
				<div class="grid__primary-container">
					<img src="/drum-digital-census-elite-2014.jpg">
					<img src="/drum-indie-agencies-census.jpg">
					<img src="/drum-network-award-2014.png">
					<img src="/drum-search-awards-finalist-2014.jpg">
					<img src="/hma-awards-2014.jpg">
					<img src="/ipa-cpd.jpg">
					<img src="/rar-finalist-award-2014.jpg">
					<img src="/top-100-agencies-2014.jpg">
					<img src="/b2b-marcomms-top70.jpg">
					<img src="/top100econsultancy.png">
					<img src="/ora_15_fin.jpg">
				</div>
			</div>
			<div class="footer__copyright-block">
				<div class="grid__primary-container">
					<div class="grid__footer-logo">
						<img class="footer__logo" src="<?php echo get_template_directory_uri(); ?>/img/4ps-mini-logo.svg" alt="<?php bloginfo( 'name' ); ?> - Logo">
					</div>
					<div class="grid__footer-copyright">
						<div class="footer__address">
							<b>4Ps Marketing Limited: 6555145</b><br>
							<address>© 4Ps Marketing Limited, Studio 16, 8 Hornsey Street, Islington, London, <span class="footer__postcode">N7 8EG</span></address>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-ui.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/main.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.flip.min.js"></script>
    <script src="<?php echo plugins_url(); ?>/flipper/remodal.min.js"></script>
    <script src="<?php echo plugins_url(); ?>/flipper/flipper.js"></script>
    <?php wp_footer(); ?>
<script type="text/javascript">
    var __ss_noform = __ss_noform || [];
    __ss_noform.push(['baseURI', 'https://app-1MYZXA6.sharpspring.com/webforms/receivePostback/M7KwsAQA/']);
    __ss_noform.push(['endpoint', '596be9c6-4f3f-4359-bdea-28b839849af1']);
</script>
<script type="text/javascript" src="https://koi-1MYZXA6.sharpspring.com/client/noform.js?ver=1.0" ></script>

</body>
</html>
