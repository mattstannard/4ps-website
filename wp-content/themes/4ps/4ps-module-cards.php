<div class="grid__card">
    <a class="card" href="<?php echo get_permalink( get_the_ID() );?>" title="<?php the_title();?>">
        <?php $categories = get_the_category(); ?>
        <p class="card__category"><?php echo ( $categories ) ? $categories[0]->name : 'Page'; ?></p>
        <p class="card__heading"><?php echo ( get_field( '4ps_card_custom_title' ) ) ? get_field( '4ps_card_custom_title' ) : get_the_title(); ?></p>
        <p class="card__excerpt"><?php echo ( get_field( '4ps_card_custom_summary', get_the_ID() ) ) ?  get_field( '4ps_card_custom_summary' ) :  get_field( '4ps_header_introduction' ) ;?></p>
    </a>
</div>
