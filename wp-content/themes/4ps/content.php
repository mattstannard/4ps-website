<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage 4Ps
 * @since 4Ps 1.0
 */
?>
<?php $categories = get_the_category(); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="hero <?php echo ( ( is_front_page() ) ? 'hero--alternative' : ''); ?> <?php echo ( ($hideHero ) ? 'hero--none' : '');?>" role="banner">
		<div class="grid__primary-container <?php echo ( ($hideHero ) ? 'grid__hide' : '');?>" itemprop="mainContentOfPage" itemscope itemtype="http://schema.org/Article">
			<div class="grid__full">
				<div class="flex__hero flex__hero--partial">

					<?php if ( is_front_page() ) : ?>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="4Ps Marketing - Home">
						<img class="hero__logo" src="<?php echo get_template_directory_uri(); ?>/img/4ps-logo.svg" alt="<?php bloginfo( 'name' ); ?> - Logo">
					</a>
					<?php endif; ?>

					<?php if ( in_array( 'category', get_object_taxonomies( get_post_type() ) ) ) : ?>
						<span class="hero__category"><?php echo get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'twentyfourteen' ) ); ?></span>
					<?php endif; ?>

                    <?php if($categories[0]->slug == "case-studies") : ?>
                        <h1 class="hero__strapline"><?php the_title(); ?></h1>
                        <meta itemprop="name" content="<?php the_title(); ?> Case Study">
                    <?php else : ?>
                        <h1 class="hero__strapline" itemprop="name"><?php the_title(); ?></h1>
                    <?php endif; ?>

                    <p class="hero__proposition" itemprop="description"><?php echo ( get_field( '4ps_header_introduction' ) ? get_field( '4ps_header_introduction' ) : ''); ?></p>

                    <?php if ( get_field( '4ps_statistic_01' ) || get_field( '4ps_statistic_02' ) || get_field( '4ps_statistic_03' ) ) : ?>
					<div class="grid__statistics statistics">

						<?php if ( get_field( '4ps_statistic_01' ) ) : ?>
						<div class="grid__statistics-block">
							<p class="statistics__stat"><?php the_field( '4ps_statistic_01' ); ?></p>
							<p class="statistics__description"><?php the_field( '4ps_statistic_description_01' ); ?></p>
						</div>
						<?php endif; ?>

						<?php if ( get_field( '4ps_statistic_02' ) ) : ?>
						<div class="grid__statistics-block">
							<p class="statistics__stat"><?php the_field( '4ps_statistic_02' ); ?></p>
							<p class="statistics__description"><?php the_field( '4ps_statistic_description_02' ); ?></p>
						</div>
						<?php endif; ?>

						<?php if ( get_field( '4ps_statistic_03' ) ) : ?>
						<div class="grid__statistics-block">
							<p class="statistics__stat"><?php the_field( '4ps_statistic_03' ); ?></p>
							<p class="statistics__description"><?php the_field( '4ps_statistic_description_03' ); ?></p>
						</div>
						<?php endif; ?>

					</div>
					<?php endif; ?>

					<?php the_tags( '<div class="tags"><div class="tags__heading"><span class="fa fa-tag"></span> <strong>TAGS:</strong></div> ', ', ' , '</div>'); ?>
                    <div class="author">Written by <span><?php the_author(); ?></span><br><span><?php the_date("jS F Y"); ?></span></div>
                    <?php echo do_shortcode( '[addtoany]' ); ?>
					<?php if ( is_page() ) : ?>
					<a href="#comment-form" class="hero__button hero__button--<?php echo ( ( $topLevel ) ? 'light' : 'dark'); ?>" title="Get in touch with 4Ps"><?php echo (get_field('4ps_header_button') ? get_field('4ps_header_button') : 'Get in touch'); ?></a>
					<?php endif; ?>

					<a href="#main-content" class="hero__arrow" title="Go to main content"><img src="<?php echo get_template_directory_uri(); ?>/img/<?php echo ( ( $topLevel && is_page()  ) ? 'arrow-down.png' : 'arrow-down-dark.png'); ?>" alt=""></a>

				</div>
			</div>
		</div>
	</header>

	<main id="main-content">

		<nav class="section section--gutters section--alternative">
			<div class="grid__primary-container">
				<div class="grid__full">
					<?php the_breadcrumbs(); ?>
				</div>
			</div>
		</nav>

		<?php if ( get_field( '4ps_case_study_about' ) ) : ?>
		<section class="section--gutters">
			<div class="grid__primary-container">
				<div class="grid__full typography">
					<div class="article__introduction">
						<h2>About the project</h2>
						<?php the_field( '4ps_case_study_about' ); ?>
					</div>
					<hr>
				</div>
			</div>
		</section>
		<?php endif; ?>

		<section class="section section--gutters">
			<div class="grid__primary-container">
				<div class="grid__full typography">
					<?php if ( is_search() ) : ?>
					<div>
						<?php the_excerpt(); ?>
					</div>
					<?php else : ?>
					<div>
						<?php the_content(); ?>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</section>

		<?php if ( get_field( '4ps_case_study_insight' ) ) : ?>
		<section class="section section--gutters section--alternative">
			<div class="grid__primary-container">
				<div class="grid__full typography">
					<div class="article__introduction">
						<h2>Insight</h2>
						<?php the_field( '4ps_case_study_insight' ); ?>
					</div>
				</div>
			</div>
		</section>
		<?php endif; ?>
	</main>
</article>

<nav class="page-navigation section--gutters">
	<div class="grid__primary-container">
		<div class="grid__previous-link">
			<div class="page-navigation__item page-navigation__item--long page-navigation__previous">
				<?php previous_post_link( '<span class="fa fa-chevron-left"></span> %link', '%title', true ); ?>
			</div>
            <div class="page-navigation__item page-navigation__item--short page-navigation__previous">
                <?php previous_post_link( '<span class="fa fa-chevron-left"></span> %link', 'Previous', true ); ?>
            </div>
		</div>
		<div class="grid__next-link">
            <div class="page-navigation__item page-navigation__item--long page-navigation__next">
                <?php next_post_link( '%link <span class="fa fa-chevron-right"></span>', '%title', true ); ?>
            </div>
            <div class="page-navigation__item page-navigation__item--short page-navigation__next">
                <?php next_post_link( '%link <span class="fa fa-chevron-right"></span>', 'Next', true ); ?>
            </div>
		</div>
	</div>
</nav>

<?php
	the_cards( array( array( 'category', array( $categories[0]->slug ) ) ), false, false );
?>
