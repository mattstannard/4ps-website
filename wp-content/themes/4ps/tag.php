<?php
/**
 * The template for displaying Tag pages
 *
 * Used to display archive-type pages for posts in a tag.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage 4Ps
 * @since 4Ps 1.0
 */

$tag = get_queried_object();

get_header(); ?>
	<header class="hero" role="banner">
		<div class="grid__primary-container">
			<div class="grid__full">
				<div class="flex__hero flex__hero--partial">
					<h1 class="hero__strapline"><?php echo single_tag_title( '', false ); ?></h1>
					<p class="hero__proposition"><?php echo ( tag_description() ) ? tag_description() : 'Stories tagged with ' . single_tag_title( '', false ) ; ?></p>
					<a href="#comment-form" class="hero__button hero__button--dark" title="Get in touch with 4Ps"><?php echo (get_field('4ps_header_button') ? get_field('4ps_header_button') : 'Get in touch'); ?></a>
					<a href="#main-content" class="hero__arrow" title="Go to main content">
						<img src="<?php echo get_template_directory_uri(); ?>/img/arrow-down-dark.png" alt="">
					</a>
				</div>
			</div>
		</div>
	</header>
	<main id="main-content">
		<?php the_cards( array( array( 'tag', array( $tag->slug ) ) ), false ); ?>
	</main>
<?php
get_footer();
