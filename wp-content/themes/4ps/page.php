<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage 4Ps
 * @since 4Ps 1.0
 */
	get_header();
?>
<main>
<?php
	while ( have_posts() ) : the_post();
		get_template_part( 'content', 'page' );
	endwhile;
?>
</main>
<?php the_cards( array( array( 'category', array( 'case-studies' ) ) ) ); ?>
<?php
	get_footer();
