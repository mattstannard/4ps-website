/**
 *
 * @authors: Ben, Matt
 * @copyright 2014, Loft Digital, www.weareloft.com
 */

/**
 * Global scope
 */

var FourPs = FourPs || {};




(function( document, window, undefined ) {
    'use strict';

    FourPs.PrimaryNavigation = (function() {

        var
            primaryNavigation = document.getElementById('primary-navigation'),
            openBtn = document.getElementById('primary-navigation-open'),
            closeBtn = document.getElementById('primary-navigation-close'),
            bodyEl = document.querySelector('#page'),

            init = function() {
                binds();
            },

            open = function() {
                primaryNavigation.classList.add('primary-navigation--open');
                bodyEl.classList.add('primary-navigation--active');
            },

            close = function() {
                primaryNavigation.classList.remove('primary-navigation--open');
                bodyEl.classList.remove('primary-navigation--active');
            },

            binds = function() {
                openBtn.addEventListener('click', open);
                closeBtn.addEventListener('click', close);
            };

        return {
            init: init
        };

    })();

})( document, window );


document.addEventListener( 'DOMContentLoaded', function() {

    if ( document.getElementById( 'primary-navigation' ) ) {

        FourPs.PrimaryNavigation.init();
    }



    $('a[href*=#]').on( 'click', function(){
        $('html, body').animate({
            scrollTop: $( $(this).attr('href') ).offset().top
        }, 500);
        return false;
    });

});



    var timer,                    // Timer object
        st,                       // Scroll top (unit: px)
        $skybar = $( '#skybar' ); // Skybar jQuery element

    // Clear and restart timer on scroll

    $( this ).scroll( function () {
        clearTimeout( timer );
        timer = setTimeout( refresh , 150 );
    });

    var refresh = function () {

        st = $( this ).scrollTop();

        if ( st === 0 ) {

            /**
             * Scroll is at the top of page
             */

            if ( $skybar.hasClass( 'skybar--sticky' ) ) {

                if ( $skybar.hasClass( 'skybar--alternative' ) ) {
                    $skybar.animate({
                        'background-color' : 'rgba(20,20,20,0)',
                        'border-bottom-color'     : 'rgba(200,200,200,1)'
                    }, 500 );
                }

                $skybar.removeClass( 'skybar--sticky' );
            }

        } else {

            /**
             * Scroll is not at the top of page
             */

            // If skybar is not sticky - make it so!

            if ( !$skybar.hasClass( 'skybar--sticky' ) ) {

                // If not transparent to begin with - make it so!

                if ( !$skybar.hasClass( 'skybar--alternative' ) ) {
                    $skybar.css({ 'background-color' : 'transparent' });
                }

                $skybar
                    .animate({
                        'background-color' : 'rgba(20,20,20,0.95)',
                    }, 500 )
                    .css({
                        'border-bottom-color' : 'rgba(20,20,20,0)'
                    });

                $skybar.addClass( 'skybar--sticky' );
            }
        }
    };

// IE detection... sorry... but it's a really piss poor implementation of flexbox in ie10/11 not working with min-height. Only specified heights.
function isIE() { return ((navigator.appName == 'Microsoft Internet Explorer') || ((navigator.appName == 'Netscape') && (new RegExp("Trident/.*rv:([0-9]{1,}[0-9]{0,})").exec(navigator.userAgent) !== null))); }

$(document).ready(function() {
    window.viewportUnitsBuggyfill.init();

    $('input, textarea').placeholder();

    $('#blog-home-pagination a').each(function(i,a){$(a).attr('href',$(a).attr('href')+'#card-container');});

    if ( isIE() ) {
        document.querySelector( 'html' ).classList.add( 'ie' );
    }
});
