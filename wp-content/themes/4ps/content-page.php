<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage 4Ps
 * @since 4Ps 1.0
 */

$currentPage = get_page( get_the_ID() );
$topLevel = false;

if ( $currentPage->post_parent == 0 ) {
	$topLevel = true;
}

// Check for featured image
$featuredImageUrl = wp_get_attachment_url( get_post_thumbnail_id($post->ID));

$image = ( get_field( '4ps_icon' ) ? get_field( '4ps_icon' ) : '' );

$alternative = false;

if ( ( $topLevel && is_page() ) || $featuredImageUrl ) {
	$alternative = true;
}

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="hero <?php echo ( ( $alternative ) ? 'hero--alternative' : ''); ?>" role="banner" <?php echo ( $featuredImageUrl ) ? 'style="background-image: linear-gradient( to bottom, rgba(0, 0, 0, 0.5) 0%, rgba(0, 0, 0, 0.8) 100% ), url(\'' . $featuredImageUrl . '\');"' : ''; ?>>
        <meta itemprop="primaryImageOfPage" content="<?php echo ( $featuredImageUrl ) ? $featuredImageUrl : ''; ?>">
        <div class="grid__primary-container">
			<div class="grid__full">
				<div class="flex__hero">
					<?php if ( $image && !$alternative ) : ?>
					<a href="<?php echo get_permalink(); ?>" class="service__wrapper service__wrapper--page" title="<?php the_title(); ?>">
						<div class="service__disk"></div>
						<img class="service__image" src="<?php echo $image['url']; ?>" alt="<?php the_title(); ?>">
					</a>
					<?php endif; ?>
					<h1 class="hero__strapline"><?php the_title(); ?></h1>
					<p class="hero__proposition"><?php ( get_field( '4ps_header_introduction' ) ? the_field( '4ps_header_introduction' ) : '' ); ?></p>
					<div class="hero__contact-details">
						<div class="hero__contact-item">
							<span class="fa fa-phone pull-left"></span> <a class="hero__contact-link <?php echo ( $alternative ) ? 'hero__contact-link--alternative' : ''; ?>" href="tel:+442076075650" alt="Get in touch">+44 (0)207 607 5650</a>
						</div>
					</div>
					<a href="#comment-form" class="hero__button hero__button--<?php echo ( ( $alternative ) ? 'light' : 'dark'); ?>" title="Get in touch with 4Ps"><?php echo (get_field('4ps_header_button') ? get_field('4ps_header_button') : 'Get in touch'); ?></a>
					<a href="#main-content" class="hero__arrow" title="Go to main content"><img src="<?php echo get_template_directory_uri(); ?>/img/<?php echo ( ( $alternative ) ? 'arrow-down.png' : 'arrow-down-dark.png'); ?>" alt=""></a>
				</div>
			</div>
		</div>
	</header>
	<div id="main-content">
		<?php the_clients( get_the_ID(), 'dark' ); ?>
		<nav class="section section--gutters section--alternative">
			<div class="grid__primary-container">
				<div class="grid__full">
					<?php the_breadcrumbs(); ?>
				</div>
			</div>
		</nav>
		<section class="section section--gutters">
			<div class="grid__primary-container">
				<?php if ( get_field( '4ps_article_introduction' ) ) : ?>
				<div class="grid__full">
					<div class="article__introduction">
						<?php the_field( '4ps_article_introduction' ); ?>
					</div>
					<hr>
				</div>
				<?php endif; ?>
				<div class="grid__full typography">
					<?php the_content(); ?>
				</div>
			</div>
		</section>
	</div>
</article>
