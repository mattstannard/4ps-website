module.exports = function(grunt) {
    // require it at the top and pass in the grunt instance
    require('time-grunt')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            options: {
                // define a string to put between each file in the concatenated output
                separator: ';'
            },
            dist: {
                // the files to concatenate
                src: ['<%= pkg.sourceDir %>**/*.js'],
                // the location of the resulting JS file
                dest: '<%= pkg.publicDir %>js/main.js'
            }
        },

        uglify: {
            options: {
                //banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n',
                sourceMap: true
            },
            dist: {
                files: {
                    '<%= pkg.publicDir %>js/main.min.js': ['<%= concat.dist.dest %>']
                }
            }
        },

        jshint: {
            files: ['Gruntfile.js', '<%= pkg.sourceDir %>js/main.js', '<%= pkg.sourceDir %>js/plugins.js'],
            options: {
                globals: {
                    jQuery: true,
                    console: true,
                    module: true,
                    document: true
                },
                '-W099': true // Hide tabs spaces warning
            }
        },

        sass: {
            dist: {
                options: {
                    style: 'compressed',
                    sourcemap: true,
                    bundleExec: true,
                    require: 'susy'
                },
                files: {
                    '<%= pkg.publicDir %>css/main.css' : '<%= pkg.sourceDir %>sass/main.scss',
                    '<%= pkg.publicDir %>css/vendors/font-awesome/font-awesome.css' : '<%= pkg.sourceDir %>sass/vendors/font-awesome/scss/font-awesome.scss',
                }
            }
        },

        autoprefixer: {
            options: {
                browsers: ['last 2 version', 'ie 8', 'ie 9']
            },
            no_dest: {
                src: '<%= pkg.publicDir %>css/main.css' // globbing is also possible here
            }

        },

        watch: {
            js: {
                files: ['<%= pkg.sourceDir %>js/**/*.js'],
                tasks: ['jshint', 'concat', 'uglify', 'notify:jsComplete']
            },
            sass: {
                files: ['<%= pkg.sourceDir %>sass/**/*.scss'],
                tasks: ['sass','autoprefixer', 'notify:sassComplete']
            }
        },
        notify: {
            sassComplete: {
                options: {
                    title: 'Sass Notification',
                    message: 'Sass compiling complete!'
                }
            },
            jsComplete: {
                options: {
                    title: 'Grunt JS Notification',
                    message: 'Js compiling complete'
                }
            }
        },
        
    });


    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-notify');
    grunt.registerTask('compile-js', ['jshint', 'concat', 'uglify']);
    grunt.registerTask('compile-sass', ['sass', 'autoprefixer']);
    grunt.registerTask('default', ['watch']);
};
