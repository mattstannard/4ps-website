<?php
/**
 * Template Name: Home Page
 *
 * @package WordPress
 * @subpackage 4Ps
 * @since 4Ps 1.0
 */

get_header();

// Check for featured image
$featuredImageUrl = wp_get_attachment_url( get_post_thumbnail_id($post->ID));
$serviceImage01 = get_field( '4ps_icon', url_to_postid( get_field( '4ps_home_service_01_link' ) ) );
$serviceImage02 = get_field( '4ps_icon', url_to_postid( get_field( '4ps_home_service_02_link' ) ) );

?>
<header class="hero hero--alternative" role="banner" <?php echo ( $featuredImageUrl ) ? 'style="background-image: linear-gradient( to bottom, rgba(0, 0, 0, 0.5) 0%, rgba(0, 0, 0, 0.8) 100% ), url(\'' . $featuredImageUrl . '\');"' : ''; ?>>
    <meta itemprop="primaryImageOfPage" content="<?php echo ( $featuredImageUrl ) ? $featuredImageUrl : ''; ?>">
    <div class="grid__primary-container">
		<div class="grid__full">
			<div class="flex__hero">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="4Ps Marketing - Home">
					<img class="hero__logo" src="<?php echo get_template_directory_uri(); ?>/img/4ps-logo.svg" alt="<?php bloginfo( 'name' ); ?> - Logo">
				</a>
				<h1 class="hero__strapline" itemprop="headline"><?php the_title(); ?></h1>
				<p class="hero__proposition hero__proposition--alternative" itemprop="about"><?php (get_field('4ps_header_introduction') ? the_field('4ps_header_introduction') : ''); ?></p>
                <div class="hero__contact-details">
                    <div class="hero__contact-item">
                        <span class="fa fa-phone pull-left"></span> <a class="hero__contact-link <?php echo ( $featuredImageUrl ) ? 'hero__contact-link--alternative' : ''; ?>" href="tel:+442076075650" alt="Get in touch">+44 (0)207 607 5650</a>
                    </div>
                </div>
				<a href="#comment-form" class="hero__button hero__button--light" title="Get in touch with 4Ps"><?php echo (get_field('4ps_header_button') ? get_field('4ps_header_button') : 'Get in touch'); ?></a>
                <a href="#main-content" class="hero__arrow" title="Go to main content"><img src="<?php echo get_template_directory_uri(); ?>/img/arrow-down.png" alt=""></a>
			</div>
		</div>
	</div>
</header>
<main id="main-content">
	<section class="section section--gutters">
		<div class="grid__primary-container">
			<div class="grid__full">
				<h2 class="section__heading"><?php echo ( get_field( '4ps_home_what_we_do_title' ) ? get_field( '4ps_home_what_we_do_title' ) : 'What we do' ); ?></h2>
				<p class="section__intro"><?php echo ( get_field( '4ps_home_what_we_do_description' ) ? get_field( '4ps_home_what_we_do_description' ) : '' ); ?></p>
			</div>
			<div>
			<div class="grid__service grid__service--left">
				<div class="service">
					<a href="<?php echo ( get_field( '4ps_home_service_01_link' ) ? get_field( '4ps_home_service_01_link' ) : '/' ); ?>" class="service__wrapper" title="<?php echo ( get_field( '4ps_home_service_01_title' ) ? get_field( '4ps_home_service_01_title' ) : '' ); ?>">
						<div class="service__disk"></div>
						<img class="service__image" src="<?php echo $serviceImage01['url']; ?>" alt="<?php echo ( get_field( '4ps_home_service_01_title' ) ? get_field( '4ps_home_service_01_title' ) : '' ); ?>">
					</a>
					<h3 class="service__title"><a href="<?php echo ( get_field( '4ps_home_service_01_link' ) ? get_field( '4ps_home_service_01_link' ) : '/' ); ?>" class="button button--primary button--full-width"><?php echo ( get_field( '4ps_home_service_01_title' ) ? get_field( '4ps_home_service_01_title' ) : '' ); ?></a></h3>
					<p class="service__description service__description--first"><?php echo ( get_field( '4ps_home_service_01_description' ) ? get_field( '4ps_home_service_01_description' ) : '' ); ?></p>
				</div>
			</div>
			<div class="grid__service grid__service--right">
				<div class="service">
					<a href="<?php echo ( get_field( '4ps_home_service_02_link' ) ? get_field( '4ps_home_service_02_link' ) : '/' ); ?>" class="service__wrapper" title="<?php echo ( get_field( '4ps_home_service_02_title' ) ? get_field( '4ps_home_service_02_title' ) : '' ); ?>">
						<div class="service__disk"></div>
						<img class="service__image" src="<?php echo $serviceImage02['url']; ?>" alt="<?php echo ( get_field( '4ps_home_service_02_title' ) ? get_field( '4ps_home_service_02_title' ) : '' ); ?>">
					</a>
					<h3 class="service__title"><a href="<?php echo ( get_field( '4ps_home_service_02_link' ) ? get_field( '4ps_home_service_02_link' ) : '/' ); ?>" class="button button--primary button--full-width"><?php echo ( get_field( '4ps_home_service_02_title' ) ? get_field( '4ps_home_service_02_title' ) : '' ); ?></a></h3>
					<p class="service__description service__description"><?php echo ( get_field( '4ps_home_service_02_description' ) ? get_field( '4ps_home_service_02_description' ) : '' ); ?></p>
				</div>
				</div>
			</div>
			<div class="grid__full">
				<hr>
				<?php the_clients( get_the_ID(), 'light', false); ?>
				<hr>
				<div class="grid__case-studies-button">
                    <a href="<?php echo get_category_link( 4 ); ?>" class="button button--primary button--full-width">View Case Studies</a>
				</div>
			</div>
		</div>
	</section>
</main>
<?php the_cards(false, true, true); ?>
<?php get_footer();
