<?php
/**
 * Template Name: Contact Us
 *
 * @package WordPress
 * @subpackage 4Ps
 * @since 4Ps 1.0
 */

get_header( 'ContactPage' );
?>
// style below added when Canterbury office removed, to override left float of address
<style>
	.grid__contact-address{clear:both;width:100%;float:none;margin-left:0;margin-right:0}
</style>
<script>

function initialize() {

    var isDraggable = true;
    if (Modernizr.touch) {
        isDraggable = false;
    }

    var londonAddress = '<div class="info-window">';
        londonAddress += '<span class="info-window__heading"><?php echo get_field( '4ps_london_name' );?></span>';
        londonAddress += '<span class="info-window__location"><?php echo get_field( '4ps_london_location' );?></span>';
        londonAddress += '<a class="info-window__directions" target="_blank" href="//maps.google.com?saddr=Current+Location&daddr=51.551143,-0.113966">Get directions <span class="fa fa-external-link pull-right"></span></a>';
        londonAddress += '</div>';

    // Create locations, an empty bounds obj, map options and finally the map
    var locations = [
        ['4Ps London', new google.maps.LatLng( 51.551143,-0.113966 ), londonAddress]
    ],

    bounds = new google.maps.LatLngBounds(),

    mapOptions = {
        //center: new google.maps.LatLng( 51.412360, 0.522184 ),
	center: new google.maps.LatLng( 51.551143,-0.113966 ),
        scrollwheel: false,
        draggable: isDraggable,
	zoom: 11
    },

    map = new google.maps.Map( document.getElementById('contact-map'), mapOptions ),

    infowindow = '';

    function createMarker( title, markerLatLng, content ) {
        var marker = new google.maps.Marker({
            position: markerLatLng,
            animation: google.maps.Animation.DROP,
            map: map,
            title: title
        });

        google.maps.event.addListener( marker, 'click', function() {

            // Close last open info window

            if (infowindow) {
                infowindow.close();
            }

            // Set info window content

            infowindow = new google.maps.InfoWindow({
              content: content
            });

            // Open info window

            infowindow.open( map, marker );
        });

        bounds.extend( marker.position );
    }

    // Loop through locations and add markers

    for ( i = 0; i < locations.length; i++ ) {
        createMarker( locations[i][0], locations[i][1], locations[i][2] );
    }

    // Adjust the map to the bounds set by the markers
    // line below removed as it was totally zooming map. Should it be?
    //map.fitBounds(bounds);

    // By default fitBounds is slightly more zoomed in than we would like, so zoom out one level
    google.maps.event.addListenerOnce(map, 'zoom_changed', function() {
        map.setZoom(map.getZoom() - 1);
    });

}

function loadScript() {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&' +
      'callback=initialize';
    document.body.appendChild(script);
}

window.onload = loadScript;
</script>
    <header class="hero" role="banner">
        <div class="grid__primary-container" itemprop="mainContentOfPage" itemscope itemtype="http://schema.org/ItemList">
            <div class="grid__full">
                <div class="flex__hero flex__hero--partial">
                    <h1 class="hero__strapline"><?php the_title(); ?></h1>
                    <meta itemprop="name" content="Office Contacts for 4Ps Marketing">
                    <div class="flex__contact flex__mobile-order-2">
                        <div class="grid__contact-address" itemprop="itemListElement" itemscope itemtype="http://schema.org/Place">
                            <meta itemprop="name" content="4Ps Marketing City Office">
                            <address class="contact__address" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                                <?php echo ( get_field( '4ps_london_address_line_1' ) ? get_field( '4ps_london_address_line_1' ).', ' : '' ); ?>
                                <span itemprop="streetAddress">
                                    <?php echo ( get_field( '4ps_london_address_line_2' ) ? get_field( '4ps_london_address_line_2' ).', ' : '' ); ?>
                                    <?php echo ( get_field( '4ps_london_address_line_3' ) ? get_field( '4ps_london_address_line_3' ).', ' : '' ); ?>
                                    <?php echo ( get_field( '4ps_london_address_line_4' ) ? get_field( '4ps_london_address_line_4' ).', ' : '' ); ?>
                                </span>
                                <span itemprop="addressLocality">
                                    <?php echo ( get_field( '4ps_london_address_city' ) ? get_field( '4ps_london_address_city' ).', ' : '' ); ?>
                                </span>
                                <span itemprop="addressRegion">
                                    <?php echo ( get_field( '4ps_london_address_county' ) ? get_field( '4ps_london_address_county' ).', ' : '' ); ?>
                                </span>
                                <span itemprop="postalCode">
                                    <?php echo ( get_field( '4ps_london_address_postcode' ) ? get_field( '4ps_london_address_postcode' ) : '' ); ?>
                                </span>
                            </address>
                        </div>
                    </div>
                    <div class="flex__contact flex__mobile-order-1">
                        <div class="grid__contact-details contact-details">
                            <ul class="contact-details__list">
                            <?php if ( get_field( '4ps_london_telephone' ) ) : ?>
                            <li class="contact-details__item"><a title="Call our London office" href="tel:<?php echo get_field( '4ps_london_telephone' );?>"><?php echo get_field( '4ps_london_telephone' );?></a></li>
                            <?php endif; ?>
                            <?php if ( get_field( '4ps_london_email' ) ) : ?>
                            <li class="contact-details__item"><a title="Email our London office" href="mailto:<?php echo get_field( '4ps_london_email' );?>"><?php echo get_field( '4ps_london_email' );?></a></li>
                            <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                    <a href="#main-content" class="hero__arrow flex__mobile-order-3" title="Go to main content"><img src="<?php echo get_template_directory_uri(); ?>/img/arrow-down-dark.png" alt=""></a>
                </div>
            </div>
        </div>
    </header>
    <main id="main-content">
        <div id="contact-map" class="contact-map"></div>
    </main>
<?php
get_footer();
