<?php
/**
 * Template Name: Our People
 *
 * @package WordPress
 * @subpackage 4Ps
 * @since 4Ps 1.0
 */

get_header(); ?>
    <main>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <header class="hero" role="banner">
            <div class="grid__primary-container">
                <div class="grid__full">
                    <div class="flex__hero">
                        <a href="<?php echo get_permalink(); ?>" class="service__wrapper" title="<?php the_title(); ?>">
                            <div class="service__disk"></div>
                            <img class="service__image" src="<?php echo ( get_field( '4ps_icon' ) ? get_field( '4ps_icon' ) : '' ); ?>" alt="<?php the_title(); ?>">
                        </a>
                        <h1 class="hero__strapline"><?php the_title(); ?></h1>
                        <p class="hero__proposition"><?php echo ( get_field( '4ps_header_introduction' ) ? get_field( '4ps_header_introduction' ) : ''); ?></p>
                        <a href="#comment-form" class="hero__button hero__button--dark" title="Get in touch with 4Ps"><?php echo ( get_field( '4ps_header_button' ) ? get_field( '4ps_header_button' ) : 'Get in touch' ); ?></a>
                        <a href="#main-content" class="hero__arrow" title="Go to main content"><img src="<?php echo get_template_directory_uri(); ?>/img/arrow-down-dark.png" alt=""></a>
                    </div>
                </div>
            </div>
        </header>
        <div id="main-content">
            <section class="section section--gutters section--alternative">
                <div class="grid__primary-container">
                <?php
                    $args = array(
                        'numberposts' => 9,
                        'orderby' => 'post_date',
                        'order' => 'DESC',
                        'post_type' => 'post',
                        'post_status' => 'publish',
                        'suppress_filters' => true,
                    );

                    $query = new WP_User_Query( $args );


                    if ( ! empty( $query->results ) ) :
                    	foreach ( $query->results as $user ) :

                            //var_dump ( $user );

                            $keyPlayer = get_field( '4ps_user_key_player' , 'user_'.$user->ID );
                            $jobTitle = get_field( '4ps_user_job_title' , 'user_'.$user->ID );

                            if ( $keyPlayer ) : ?>

                                <div class="grid__card">
                                    <div class="card">

                                        <?php echo get_wp_user_avatar($user->ID, 150); ?>

                                        <p class="card__category"><?php echo $jobTitle; ?></p>
                                        <p class="card__heading"><?php echo $user->display_name; ?></p>
                                        <p class="card__excerpt">
                                        <?php
                                        if ( get_the_author_meta('description', $user->ID) ) : // If a user has filled out their decscription show a bio on their entries
                                            echo get_the_author_meta('description', $user->ID);
                                        endif;
                                        ?>
                                        </p>
                                    </div>
                                </div>
                            <?php endif;
                        endforeach;
                    endif;
                ?>
                </div>
            </section>
        </div>
    </article>
</main>
<?php get_footer();
