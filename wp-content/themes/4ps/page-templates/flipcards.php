<?php
/**
 * Template Name: Flip Cards
 *
 * @package WordPress
 * @subpackage 4Ps
 * @since 4Ps 1.0
 */

get_header();
// Check for featured image
$featuredImageUrl = wp_get_attachment_url( get_post_thumbnail_id($post->ID));

?>
<header class="hero hero--alternative" role="banner" <?php echo ( $featuredImageUrl ) ? 'style="background-image: linear-gradient( to bottom, rgba(0, 0, 0, 0.5) 0%, rgba(0, 0, 0, 0.8) 100% ), url(\'' . $featuredImageUrl . '\');"' : ''; ?>>
    <meta itemprop="primaryImageOfPage" content="<?php echo ( $featuredImageUrl ) ? $featuredImageUrl : ''; ?>">
    <div class="grid__primary-container" itemprop="mainContentOfPage">
        <div class="grid__full">
            <div class="flex__hero">
                <h1 class="hero__strapline"><?php the_title(); ?></h1>
                <p class="hero__proposition hero__proposition--alternative"><?php echo ( get_field( '4ps_header_introduction' ) ? get_field( '4ps_header_introduction' ) : ''); ?></p>
                <a href="#main-content" class="hero__arrow" title="Go to main content"><img src="<?php echo get_template_directory_uri(); ?>/img/arrow-down.png" alt=""></a>
            </div>
        </div>
    </div>
</header>
<main id="main-content">
    <nav class="section section--gutters section--alternative">
        <div class="grid__primary-container">
            <div class="grid__full">
                <?php the_breadcrumbs(); ?>
            </div>
        </div>
    </nav>
    <?php flipcards( get_the_category() ); ?>
</main>
<?php
get_footer();
