<?php
/**
 * Template Name: Section
 *
 * @package WordPress
 * @subpackage 4Ps
 * @since 4Ps 1.0
 */

get_header();

// Check for featured image
$featuredImageUrl = wp_get_attachment_url( get_post_thumbnail_id($post->ID));

?>
<main>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <header class="hero hero--alternative" role="banner" <?php echo ( $featuredImageUrl ) ? 'style="background-image: linear-gradient( to bottom, rgba(0, 0, 0, 0.5) 0%, rgba(0, 0, 0, 0.8) 100% ), url(\'' . $featuredImageUrl . '\');"' : ''; ?>>
            <meta itemprop="primaryImageOfPage" content="<?php echo ( $featuredImageUrl ) ? $featuredImageUrl : ''; ?>">
            <div class="grid__primary-container" itemprop="mainContentOfPage">
                <div class="grid__full">
                    <div class="flex__hero">
                        <h1 class="hero__strapline"><?php the_title(); ?></h1>
                        <p class="hero__proposition hero__proposition--alternative"><?php echo ( get_field( '4ps_header_introduction' ) ? get_field( '4ps_header_introduction' ) : ''); ?></p>
                        <div class="hero__contact-details">
                            <div class="hero__contact-item">
                                <span class="fa fa-phone pull-left"></span> <a class="hero__contact-link <?php echo ( $featuredImageUrl ) ? 'hero__contact-link--alternative' : ''; ?>" href="tel:+442076075650" alt="Get in touch">+44 (0)207 607 5650</a>
                            </div>
                        </div>
                        <a href="#comment-form" class="hero__button hero__button--light" title="Get in touch with 4Ps"><?php echo ( get_field( '4ps_header_button' ) ? get_field( '4ps_header_button' ) : 'Get in touch' ); ?></a>
                        <a href="#main-content" class="hero__arrow" title="Go to main content"><img src="<?php echo get_template_directory_uri(); ?>/img/arrow-down.png" alt=""></a>
                    </div>
                </div>
            </div>
        </header>
        <div id="main-content">
            <nav class="section section--gutters section--alternative">
                <div class="grid__primary-container">
                    <div class="grid__full">
                        <?php the_breadcrumbs(); ?>
                    </div>
                </div>
            </nav>
        <?php
            $currentId = get_the_ID();

            $args = array(
                'post_parent'   => get_the_ID(),
                'post_type'     => 'page',
                'orderby'       => 'menu_order',
                'order'         => 'ASC'
            );

            $query = new WP_Query( $args );

            if ( $query->have_posts() ) :

                $i = 0;

                while ( $query->have_posts() ) : $query->the_post();

                $image = ( get_field( '4ps_icon' ) ? get_field( '4ps_icon' ) : '' );
                ?>
                <section class="section--gutters <?php echo ( ( $i % 2 ) != 0 ) ? 'section--alternative' : ''; ?>">
                    <div class="grid__primary-container">
                        <div class="grid__section-child-image grid__section-child-image--<?php echo ( ( $i % 2 ) != 0 ) ? 'odd' : 'even'; ?>">
                            <div class="flex__services">
                                <?php if ( $image ) : ?>
                                <a href="<?php echo get_permalink(); ?>" class="service__wrapper" title="<?php the_title(); ?>">
                                    <div class="service__disk"></div>
                                    <img class="service__image" src="<?php echo $image['url']; ?>" alt="<?php the_title(); ?>">
                                </a>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="grid__section-child-text grid__section-child-text--<?php echo ( ( $i % 2 ) != 0 ) ? 'odd' : 'even'; ?>">
                            <div class="flex__services">
                                <header class="flex__services-header">
                                    <h2 class="child-section__heading child-section__heading--<?php echo ( ( $i % 2 ) != 0 ) ? 'odd' : 'even'; ?>"><?php echo ( get_field( '4ps_child_title' ) ? get_field( '4ps_child_title' ) : '' ); ?></h2>
                                    <p class="child-section__subheading child-section__subheading--<?php echo ( ( $i % 2 ) != 0 ) ? 'odd' : 'even'; ?>"><?php echo ( get_field( '4ps_child_tagline' ) ? get_field( '4ps_child_tagline' ) : '' ); ?></p>
                                </header>
                                <div class="flex__services-content">
                                    <div class="child-section__introduction child-section__introduction--<?php echo ( ( $i % 2 ) != 0 ) ? 'odd' : 'even'; ?>"><?php echo ( get_field( '4ps_child_description' ) ? get_field( '4ps_child_description' ) : '' ); ?></div>
                                </div>
                                <div class="flex__services-button grid__section-child-button grid__section-child-button--<?php echo ( ( $i % 2 ) != 0 ) ? 'odd' : 'even'; ?>">
                                    <a class="child-section__button button--full-width" href="<?php echo get_permalink(); ?>" title="View more about <?php echo the_title();?>">Learn more</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>

                <?php
                $i++;
                endwhile;

                wp_reset_postdata();

            endif;
            ?>
            <section class="section section--gutters <?php echo ( ( $i % 2 ) != 0 ) ? 'section--alternative' : ''; ?>">
                <div class="grid__primary-container">
                    <div class="typography">
                        <?php the_content(); ?>
                    </div>
                </div>
            </section>
        </div>
    </article>
    <?php the_clients( get_the_ID(), 'dark' ); ?>
</main>
<?php the_cards( array( array( 'category', array( 'case-studies' ) ) ), true, false ); ?>
<?php
get_footer();
