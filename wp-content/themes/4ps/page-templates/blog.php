<?php
/**
 * Template Name: Blog
 *
 * @package WordPress
 * @subpackage 4Ps
 * @since 4Ps 1.0
 */

get_header(); ?>

<header class="hero" role="banner">
    <div class="grid__primary-container">
        <div class="grid__full">
            <div class="flex__hero flex__hero--partial">
                <h1 class="hero__strapline"><?php the_title(); ?></h1>
                <p class="hero__proposition"><?php echo ( get_field( '4ps_header_introduction' ) ? get_field( '4ps_header_introduction' ) : ''); ?></p>
                <div class="grid__statistics statistics">
                <?php
                    $categories = get_categories();
                    $i = 0;

                    $categories = array_filter($categories, 'myFilter');

                    foreach($categories as $category):
                            $iconImage = "/4ps-news.png";
                            if ($category->name == "4Ps News") {
                                $iconImage = "/4ps-news.png";
                            }
                            else if ($category->name == "Search Industry News"){
                                $iconImage = "/industry-sector.png";
                            }
                            else if ($category->name == "Market Sectors"){
                                $iconImage = "/search-news.png";
                            }
                            else if ($category->name == "4Ps Labs"){
                                $iconImage = "/4ps-labs.png";
                            }
                            else if ($category->name == "Knowledge"){
                                $iconImage = "/search-knowledge.png";
                            }
                            else if ($category->name == "Case Studies"){
                                $iconImage = "/case-studies.png";
                            }
			    //Following two elseif's miss out client and people, at request of Ruth.
			    else if ($category->name == "Clients"){
                               continue;
                            }
			    else if ($category->name == "People"){
                               continue;
                            }

                            //echo "<img class='hero__icon' alt='".$category->name."' src='".$iconImage."'/>";
                        ?>
                        <span class="hero__category"><?php echo "<img class='hero__icon' alt='".$category->name."' src='".$iconImage."'/>"; ?><a href="<?php echo get_category_link( $category->term_id ); ?>" title="<?php echo $category->description; ?>">
                                <?php echo $category->name; ?>
                            </a></span>
                        <?php if( $i < ( count( $categories ) -1 ) ) : ?>
                            <span style="margin: 0 10px;">/</span>
                        <?php endif; ?>
                        <?php
                        $i++;
                    endforeach;
                ?>
                </div>
                <?php get_search_form(); ?>
                <a href="#main-content" class="hero__arrow" title="Go to main content"><img src="<?php echo get_template_directory_uri(); ?>/img/arrow-down-dark.png" alt=""></a>
            </div>
        </div>
    </div>
</header>
<main id="main-content">
    <nav class="section section--gutters section--alternative">
        <div class="grid__primary-container">
            <div class="grid__full">
                <?php the_breadcrumbs(); ?>
            </div>
        </div>
    </nav>
    <?php the_cards( false, false ); ?>
</main>
<?php
function myFilter($string) {
    return strpos($string->name, '-') === false;
}


get_footer();
