<?php
/**
 * The template for displaying Category pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage 4Ps
 * @since 4Ps 1.0
 */

get_header();

$cat = get_category( get_query_var( 'cat' ) );
?>
	<header class="hero" role="banner">
		<div class="grid__primary-container">
			<div class="grid__full">
				<div class="flex__hero flex__hero--partial">
					<h1 class="hero__strapline"><?php echo $cat->name; ?></h1>
					<p class="hero__proposition"><?php echo $cat->category_description; ?></p>
					<?php get_search_form(); ?>
					<a href="#main-content" class="hero__arrow" title="Go to main content">
						<img src="<?php echo get_template_directory_uri(); ?>/img/arrow-down-dark.png" alt="">
					</a>
				</div>
			</div>
		</div>
	</header>
	<main id="main-content">
		<nav class="section section--gutters section--alternative">
			<div class="grid__primary-container">
				<div class="grid__full">
					<?php the_breadcrumbs(); ?>
				</div>
			</div>
		</nav>
		<?php the_cards( array( array( 'category', array( $cat->slug ) ) ), false ); ?>
	</main>
<?php
get_footer();
