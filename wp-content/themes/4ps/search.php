<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage 4Ps
 * @since 4Ps 1.0
 */

get_header( 'SearchResultsPage' );
$searchTerm = trim( get_search_query() );
?>
<header class="hero" role="banner">
	<div class="grid__primary-container" itemprop="mainContentOfPage">
        <div class="grid__full">
			<div class="flex__hero flex__hero--partial">
				<h1 class="hero__strapline">
					<?php
						if ( $searchTerm && have_posts() ) :
							echo 'Search Results';
						elseif ( $searchTerm && !have_posts() ) :
							echo 'No Results Found';
						else :
							echo 'Search';
						endif;

						echo $post->post_count;
					?>
				</h1>
				<p class="hero__proposition"><?php  echo ( $searchTerm ) ? 'Results for: ' . $searchTerm : 'Looking for something in particular?'; ?></p>
				<div class="grid__statistics statistics">
					<?php
						$categories = get_categories();
						$i = 0;

						foreach($categories as $category):
						?>
							<span class="hero__category"><a href="<?php echo get_category_link( $category->term_id ); ?>" title="<?php echo $category->description; ?>"><?php echo $category->name; ?></a></span>
							<?php if( $i < ( count( $categories ) -1 ) ) : ?>
								<span style="margin: 0 10px;">/</span>
							<?php endif; ?>
						<?php
						$i++;

						endforeach;
					?>
				</div>
				<?php get_search_form(); ?>
				<a href="#main-content" class="hero__arrow" title="Go to main content"><img src="<?php echo get_template_directory_uri(); ?>/img/arrow-down-dark.png" alt=""></a>
			</div>
		</div>
	</div>
</header>

<main id="main-content">
	<nav class="section section--gutters section--alternative">
		<div class="grid__primary-container">
			<div class="grid__full">
				<?php the_breadcrumbs(); ?>
			</div>
		</div>
	</nav>
	<?php if ( $searchTerm && have_posts() ) : ?>

	<?php else : ?>
		<?php $query_string = ''; ?>
	<?php endif; ?>
	<?php the_cards( false, false ); ?>
</main>
<?php
get_footer();
